int factorial(int n) {
  if(n == 0) {
    return 1 ;
  } else {
    return n * factorial(n - 1) ;
  }
}

void main() {
  int x = rand(0,10) ;
  x = factorial(x) ;
}
