(*
  Inspiré de :
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

let main () =
  Arg.parse Settings.options (fun s -> Settings.files := String.split_on_char ' ' s) Settings.usage ;

  (* Choix des modules utilisés *)
  let select_value_domain : Settings.value_domain -> (module Value_domain.VALUE_DOMAIN) = function
    | Constants -> (module Value_domain.Constants)
    | Intervals -> (module Value_domain.Intervals)
    | DisjointIntervals -> (module Value_domain.DisjointIntervals) in
  let v = select_value_domain !Settings.value_domain in
  let module V = (val v) in

  let select_domain : Settings.domain -> (module Domain.DOMAIN) = function
    | NonRelational -> (module Domain.MakeNonRelational(V))
    | Equality -> (module Domain.Equality)
    | Product -> (module Domain.MakeProduct(Domain.Equality)(Domain.MakeNonRelational(V)) )
  in
  let d = select_domain !Settings.domain in
  let module D = (val d) in

  let select_iterator : Settings.iterator -> (module Interpreter.ITERATOR) = function
    | Worklist -> (module Interpreter.MakeWorklist(D)) in
  let i = select_iterator !Settings.iterator in
  let module I = (val i) in

  (* Analyse d'un fichier *)
  let analyze file =
    let prog = File_parser.parse_file file in
    let cfg = Tree_to_cfg.prog prog in

    Format.printf "-------------------- %s --------------------@." file ;
    Format.printf "%a" Cfg_printer.print_cfg cfg;
    let file_prefix = (Filename.chop_suffix file ".c") in
    let dot_file = file_prefix ^ ".dot" in
    Cfg_printer.output_dot dot_file cfg ;

    if !Settings.print_as_pdf then
      begin
        let pdf_file = file_prefix ^ ".pdf" in
        ignore (Sys.command (Format.sprintf "dot -Tps %s -o %s" dot_file pdf_file)) ;
        Sys.remove dot_file ;
      end ;

    if not !Settings.print_only then
      begin
        let envs = (try
                      I.eval_prog cfg
                    with
                    | Interpreter.Error (s, ext) ->
                      Printer.report ext file ;
                      Format.eprintf "Error : %s@." s ;
                      exit 2) in
        Format.printf "%a" I.print_envs (cfg, envs) ;
        Format.printf "%a" I.print_asserts (cfg, envs)
      end in

  match !Settings.files with
  | [] -> failwith "no source file specified"
  | files -> List.iter analyze files

let _ = main ()
