(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2014
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

val parse_file: string -> Ast.prog
(* Opens and parses a file given as argument. *)

