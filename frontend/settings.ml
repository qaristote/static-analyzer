type value_domain = Constants | Intervals | DisjointIntervals
type domain = NonRelational | Equality | Product
type iterator = Worklist

let value_domain = ref Intervals
let domain = ref NonRelational
let iterator = ref Worklist

let print_only = ref false
let print_as_pdf = ref false

let files : string list ref = ref []

(* Les options de l'analyseur affichées avec --help *)
let specify_value_domain = Arg.String (fun s -> value_domain := match String.lowercase_ascii s with
    | "constants" -> Constants
    | "intervals" -> Intervals
    | "disjoint-intervals" -> DisjointIntervals
    | _ -> failwith "unsupported value domain")

let specify_domain = Arg.String (fun s -> domain := match String.lowercase_ascii s with
    | "non-relational" -> NonRelational
    | "equality" -> Equality
    | "product" -> Product
    | _ -> failwith "unsupported domain")

let specify_iterator = Arg.String (fun s -> domain := match String.lowercase_ascii s with
    | "worklist" -> NonRelational
    | _ -> failwith "unsupported iterator")

let set_print_only = Arg.Set print_only
let set_print_as_pdf = Arg.Set print_as_pdf

let options =
  Arg.align [
    "-vd",             specify_value_domain,  " Specify the value domain (default : intervals)" ;
    "--value_domain",  specify_value_domain,  " available : constants, intervals, disjoint-intervals\n" ;

    "-d",              specify_domain,        " Specify the domain (default : non-relational)" ;
    "--domain",        specify_domain,        " available : non-relational, equality, product\n" ;

    "-i",              specify_iterator,      " Specify the iterator (default : worklist)" ;
    "--iterator",      specify_iterator,      " available : worklist\n" ;
    
    "-p",              set_print_only,        " Only print the resulting CFG" ;
    "--print-only",    set_print_only,        " \n" ;

    "-pdf",            set_print_as_pdf,      " Directly print the CFG as a .pdf file instead of a .dot one" ;
    "--print-as-pdf",  set_print_as_pdf,      " \n"
  ]

let usage = "usage : analyzer.bc [OPTION]... [FILE]..."
