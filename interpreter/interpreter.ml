open! Cfg
open Ast
open Domain

exception Error of string ext
                   
module type ITERATOR =
sig
  module D : DOMAIN
  type env = D.t
  val eval_prog : cfg -> env NodeMap.t
  val print_envs : Format.formatter -> cfg * env NodeMap.t -> unit
  val print_asserts : Format.formatter -> cfg * env NodeMap.t -> unit
end
  
module MakeWorklist(D : DOMAIN) : ITERATOR =
  (struct
    module D = D
    type env = D.t

    let rec eval_stat (env : env) : inst -> env = function
      | CFG_skip _ -> env
      | CFG_assign (var, expr) -> D.assign env var expr
      | CFG_guard expr -> D.guard env expr
      | CFG_assert expr -> D.guard env expr
      | CFG_call _ -> raise (Invalid_argument "eval_stat") (* Should not happen since we replaced every function call by gotos *)
              
    and update_worklist (worklist : NodeSet.t) (node : node) (update : bool) : NodeSet.t =
      NodeSet.remove node (if update then
                             List.fold_left
                               (fun tmp_worklist arc -> NodeSet.add arc.arc_dst tmp_worklist)
                               worklist
                               node.node_out
                           else
                             worklist)
        
    and update_node (envs : env NodeMap.t) (node : node) : env * bool=
      let old_env = NodeMap.find node envs in
      let new_env = List.fold_left
                      (fun tmp_env arc ->
                         try
                           D.widen
                             tmp_env
                             (eval_stat (NodeMap.find arc.arc_src envs) arc.arc_inst)
                         with
                           | Domain.Error s -> raise (Error (s, (arc.arc_src.node_pos,
                                                                 arc.arc_dst.node_pos))))
                      old_env
                      node.node_in in
      new_env, not (D.subset new_env old_env)

    and update_nodes (envs : env NodeMap.t) (worklist : NodeSet.t) : env NodeMap.t =
      let rec aux envs worklist =
        if NodeSet.is_empty worklist then
          envs, worklist
        else (
          let node = NodeSet.choose worklist in
          let env, updated = update_node envs node in
          aux (NodeMap.add node env envs) (update_worklist worklist node updated)) in
      fst (aux envs worklist)
      
    let eval_prog (prog : cfg) : env NodeMap.t =
      let envs = NodeMap.add
                   prog.cfg_init_entry
                   (D.init prog.cfg_vars)
                   (List.fold_left
                      (fun tmp_envs node -> NodeMap.add node D.bottom tmp_envs)
                      NodeMap.empty
                      prog.cfg_nodes) in
      let worklist = update_worklist
                       (NodeSet.singleton prog.cfg_init_entry)
                       prog.cfg_init_entry
                       true in
      update_nodes envs worklist

    let print_envs (fmt : Format.formatter) (prog, envs : cfg * env NodeMap.t) : unit =
      Format.fprintf fmt "List of environments :@\n  " ;
      List.iter
        (fun node -> Format.fprintf fmt "@[@[%i : %a@]@\n"
                       node.node_id
                       D.print (NodeMap.find node envs))
        (List.find (fun f -> f.func_name = "main") prog.cfg_funcs).func_nodes ;
      Format.fprintf fmt "@]@."

    let rec eval_bool_expr (env : D.t) : Cfg.bool_expr -> bool * bool = function
      | CFG_bool_const b -> b, not b
      | CFG_bool_rand -> true, true
      | CFG_bool_unary (_, expr) -> let b1, b2 = eval_bool_expr env expr in not b1, not b2
      | CFG_bool_binary (op, expr1, expr2) ->
          let b11, b12 = eval_bool_expr env expr1
          and b21, b22 = eval_bool_expr env expr2 in
          begin
            match op with
              | Ast.AST_AND -> b11 && b21, b12 || b22
              | Ast.AST_OR -> b11 || b21, b12 && b22
          end
      | CFG_compare (op, expr1, expr2) -> D.eval_compare env op expr1 expr2

    let print_asserts (fmt : Format.formatter) (prog, envs : cfg * env NodeMap.t) : unit =
      Format.fprintf fmt "List of assertions :@\n  " ;
      List.iter
        (fun arc -> match arc.arc_inst with
            | CFG_assert expr ->
                let env = NodeMap.find arc.arc_src envs in
                Format.fprintf fmt "@[@[%i -> %i@] : @[%a ≡ %s@]@\n"
                  arc.arc_src.node_id
                  arc.arc_dst.node_id
                  Cfg_printer.print_inst arc.arc_inst
                  (match eval_bool_expr env expr with
                    | false, true -> "false"
                    | true, false -> "true"
                    | _ -> "unknown")
            | _ -> ())
        prog.cfg_arcs ;
      Format.fprintf fmt "@]@."
  end)
