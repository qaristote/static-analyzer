# Static Analyzer

This project is part of the *Sémantique et Vérification* course at the *École Normale Supérieure 
de Paris*. Its aim is to analyze programs in C and check that they verify some assertions (amongst
others, it makes sure that the program cannot raise an exception).

# Usage 

```
usage : analyzer.bc [OPTION]... [FILE]...
  -vd              Specify the value domain (default : intervals)
  --value_domain   available : constants, intervals, disjoint-intervals

  -d               Specify the domain (default : non-relational)
  --domain         available : non-relational, equality, product

  -i               Specify the iterator (default : worklist)
  --iterator       available : worklist

  -p               Only print the resulting CFG
  --print-only

  -pdf             Directly print the CFG as a .pdf file instead of a .dot one
  --print-as-pdf

  -help            Display this list of options
  --help           Display this list of options
```
