all:
	@rm analyzer.bc -f
	@dune build analyzer.bc
	@ln -s _build/default/analyzer.bc analyzer.bc

clean:
	@dune clean
	@rm analyzer.bc
