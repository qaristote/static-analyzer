open Ast
exception Error of string

module Bound =
struct
  type t = Inf | Int of Z.t | Sup

  let zero      : t = Int Z.zero
  let one       : t = Int Z.one
  let minus_one : t = Int Z.minus_one

  let compare (b1 : t) (b2 : t) : int = match b1, b2 with
    | b1, b2 when b1 = b2 -> 0
    | Inf, _ | _, Sup -> -1
    | Sup, _ | _, Inf -> 1
    | Int m, Int n -> Z.compare m n

  let neg : t -> t = function
    | Inf -> Sup
    | Int n -> Int (Z.neg n)
    | Sup -> Inf

  let add (b1 : t) (b2 : t) = match b1, b2 with
    | Inf, Sup | Sup, Inf -> Int (Z.zero)
    | Inf, _ | _, Inf -> Inf
    | Sup, _ | _, Sup -> Sup
    | Int m, Int n -> Int (Z.add m n)

  let sub (b1 : t) (b2 : t) = add b1 (neg b2)

  let mul (b1 : t) (b2 : t) = match b1, b2 with
    | b1, b2 when b1 = zero || b2 = zero -> zero
    | Inf, b | b, Inf -> if compare b zero < 0 then Sup else Inf
    | Sup, b | b, Sup -> if compare b zero >= 0 then Sup else Inf
    | Int m, Int n -> Int (Z.mul m n)

  let div (b1 : t) (b2 : t) = match b1, b2 with
    | Inf, Inf | Sup, Sup -> one
    | Inf, Sup | Sup, Inf -> minus_one
    | Inf, b -> if compare b zero < 0 then Sup else Inf
    | Sup, b -> if compare b zero >= 0 then Sup else Inf
    | _, (Inf | Sup) -> zero
    | Int m, Int n -> Int (Z.ediv m n)
end

module Interval =
struct
  type t = Between of Bound.t * Bound.t | Empty

  let bottom : t = Empty
  let top : t = Between (Bound.Inf, Bound.Sup)

  let const (n : Z.t) : t = Between (Bound.Int n, Bound.Int n)
  let rand (m : Z.t) (n : Z.t) : t = Between (Bound.Int m, Bound.Int n)

  let inf : t -> Bound.t = function
    | Empty -> Sup
    | Between (m, _) -> m
  let sup : t -> Bound.t = function
    | Empty -> Inf
    | Between (_, n) -> n

  let contains_zero : t -> bool = function
    | Empty -> false
    | Between (lo, hi) -> Bound.compare lo Bound.zero <= 0 && Bound.compare Bound.zero hi <= 0

  let positive_part : t -> t = function
    | Empty -> Empty
    | Between (m, n) -> Between (max m Bound.one, n)
  let negative_part : t -> t = function
    | Empty -> Empty
    | Between (m, n) -> Between (m, min n Bound.minus_one)

  let is_bottom : t -> bool = function
    | Empty -> true
    | Between (m, n) -> m > n

  let simplify (i : t) : t = if is_bottom i then Empty else i

  let list_min = List.fold_left min Bound.Sup
  let list_max = List.fold_left max Bound.Inf



  let join (i : t) (j : t) : t =
    simplify (match i, j with
        | Empty, j -> j
        | i, Empty -> i
        | Between (lo1, hi1), Between (lo2, hi2) -> Between (min lo1 lo2, max hi1 hi2))

  let meet (i : t) (j : t) : t =
    simplify (match i, j with
        | Empty, _ | _, Empty -> Empty
        | Between (lo1, hi1), Between (lo2, hi2) -> Between (max lo1 lo2, min hi1 hi2))

  let unary (i : t) : int_unary_op -> t = function
    | AST_UNARY_PLUS -> i
    | AST_UNARY_MINUS -> match i with
      | Empty -> Empty
      | Between (lo, hi) -> Between (Bound.neg hi, Bound.neg lo)

  let binary (i : t) (j : t) (op : int_binary_op) : t =
    simplify (match i, j with
        | Empty, _ | _, Empty -> Empty
        | Between (lo1, hi1), Between (lo2, hi2) -> match op with
          | AST_PLUS -> Between (Bound.add lo1 lo2, Bound.add hi1 hi2)
          | AST_MINUS -> Between (Bound.sub lo1 hi2, Bound.sub hi1 lo2)
          | AST_MULTIPLY -> let candidates = [ Bound.mul lo1 lo2 ;
                                               Bound.mul lo1 hi2 ;
                                               Bound.mul hi1 lo2 ;
                                               Bound.mul hi1 hi2 ] in
            Between (list_min candidates, list_max candidates)
          | AST_DIVIDE ->
            if contains_zero j then
              raise (Error "division by Bound.zero")
            else
              let candidates = [ Bound.div lo1 lo2 ;
                                 Bound.div lo1 hi2 ;
                                 Bound.div hi1 lo2 ;
                                 Bound.div hi1 hi2 ] in
              Between (list_min candidates, list_max candidates)
          | AST_MODULO ->
            if contains_zero j then
              raise (Error "modulo by Bound.zero")
            else
              begin
                match lo1, hi1, lo2, hi2 with
                | Bound.Inf, _, _, _ | _, Bound.Sup, _, _ -> Between (Bound.zero, Bound.Sup)
                | _, _, Bound.Inf, _ | _, _, _, Bound.Sup ->
                  if Bound.compare (max (Bound.neg lo1) hi1) (min lo2 (Bound.neg hi2)) < 0 then
                    i
                  else
                    Between (Bound.zero, max (Bound.neg lo1) hi1)
                | Int m1, Int n1, Int m2, Int n2 ->
                  let m1 = Z.max m1 (Z.neg n1)
                  and n1 = Z.min (Z.neg m1) n1
                  and m2 = Z.max m2 (Z.neg n2)
                  and n2 = Z.min (Z.neg m2) n2 in
                  let tmp_lo = ref Bound.Sup
                  and tmp_hi = ref Bound.Inf in
                  let iterator = ref m2 in
                  while Z.leq !iterator n2 do
                    if Z.leq !iterator (Z.sub n1 m1)
                    || Z.leq (Z.erem n1 !iterator) (Z.erem m1 !iterator) then (
                      tmp_lo := Bound.zero ;
                      tmp_hi := max !tmp_hi (Int !iterator)
                    ) else (
                      tmp_lo := min !tmp_lo (Int (Z.erem m1 !iterator)) ;
                      tmp_hi := max !tmp_hi (Int (Z.erem n1 !iterator))
                    ) ;
                    iterator := Z.add !iterator Z.one
                  done ;
                  Between (!tmp_lo, !tmp_hi)
                | _ -> (* should not happen *) exit 2
              end)

  let compare (i : t) (j : t) (op : compare_op) : t * t =
    let new_i, new_j = match i, j with
      | Empty, _ | _, Empty -> Empty, Empty
      | Between (lo1, hi1), Between (lo2, hi2) -> match op with
        | AST_EQUAL -> let k = meet i j in k, k
        | AST_NOT_EQUAL ->
          (if lo2 <> hi2 || (lo1 <> lo2 && hi1 <> hi2) then
             i
           else (if lo1 = lo2 then
                   Between (Bound.add Bound.one lo1, hi1)
                 else
                   Between (lo1, Bound.sub hi1 Bound.one))),
          (if lo1 <> hi1 || (lo1 <> lo2 && hi1 <> hi2) then
             j
           else (if lo1 = lo2 then
                   Between (Bound.add Bound.one lo2, hi2)
                 else
                   Between (lo2, Bound.sub hi2 Bound.one)))
        | AST_LESS -> Between (lo1, min hi1 (Bound.sub hi2 Bound.one)), Between (lo2, min (Bound.sub hi1 Bound.one) hi2)
        | AST_LESS_EQUAL -> Between (lo1, min hi1 hi2), Between (lo2, min hi1 hi2)
        | AST_GREATER -> Between (max lo1 (Bound.add lo2 Bound.one), hi1), Between (max (Bound.add lo1 Bound.one) lo2, hi2)
        | AST_GREATER_EQUAL -> Between (max lo1 lo2, hi1), Between (max lo1 lo2, hi2)
    in simplify new_i, simplify new_j

  let bwd_binary (i : t) (j : t) (op : int_binary_op) (r : t) : t * t =
    let new_i, new_j = match r with
      | Empty -> Empty, Empty
      | Between (lo, hi) -> match op with
        | AST_PLUS -> meet i (binary r j AST_MINUS), meet (binary r i AST_MINUS) j
        | AST_MINUS -> meet i (binary r j AST_PLUS), meet (binary r i AST_PLUS) j
        | AST_MULTIPLY -> let r_contains_zero = contains_zero r in
          (if r_contains_zero && contains_zero j then
             i
           else
             meet i (join
                       (binary r (negative_part j) AST_DIVIDE)
                       (binary r (positive_part j) AST_DIVIDE))),
          (if  r_contains_zero && contains_zero i then
             j
           else
             meet j (join
                       (binary r (negative_part i) AST_DIVIDE)
                       (binary r (positive_part i) AST_DIVIDE)))
        | AST_DIVIDE -> meet i (binary r j AST_MULTIPLY), meet (binary r i AST_MULTIPLY) j
        | AST_MODULO -> let new_lo = min (max lo (Bound.neg lo)) (max hi (Bound.neg hi)) in
          join
            (meet i (binary j (Between (new_lo, Bound.Sup)) AST_MULTIPLY))
            (meet i (binary j (Between (Bound.Inf, Bound.neg new_lo)) AST_MULTIPLY)),
          join
            (meet j (binary i (Between (new_lo, Bound.Sup)) AST_MULTIPLY))
            (meet j (binary i (Between (Bound.Inf, Bound.neg new_lo)) AST_MULTIPLY))
    in simplify new_i, simplify new_j

  let print (fmt : Format.formatter) : t -> unit = function
    | Empty -> Format.fprintf fmt "∅"
    | Between (lo, hi) when lo <> hi ->
      begin
        match lo with
        | Bound.Inf -> Format.fprintf fmt "]-oo"
        | Int n -> Format.fprintf fmt "[%a" Z.pp_print n
        | Bound.Sup -> Format.fprintf fmt "]+oo"
      end ;
      Format.fprintf fmt ";" ;
      begin
        match hi with
        | Bound.Inf -> Format.fprintf fmt "-oo["
        | Int n -> Format.fprintf fmt "%a]" Z.pp_print n
        | Bound.Sup -> Format.fprintf fmt "+oo["
      end ;
    | Between (lo, hi) -> match lo with
      | Bound.Inf | Bound.Sup -> Format.fprintf fmt "∅"
      | Int n -> Format.fprintf fmt "{%a}" Z.pp_print n

  let subset (i : t) (j : t) : bool = i = meet i j

  let eval_compare (op : compare_op) (m : t) (n : t) : bool * bool = match m, n with
    | Empty, _ | _, Empty -> false, false
    | Between (lo1, hi1), Between (lo2, hi2) -> match Bound.compare hi1 lo2, Bound.compare hi2 lo1, op with
      | -1, _, (AST_EQUAL | AST_GREATER | AST_GREATER_EQUAL)
      | _, -1, (AST_EQUAL | AST_LESS | AST_LESS_EQUAL) -> false, true
      | -1, _, (AST_NOT_EQUAL | AST_LESS | AST_LESS_EQUAL)
      | _, -1, (AST_NOT_EQUAL | AST_GREATER | AST_GREATER_EQUAL) -> true, false
      | 0, _, _ when lo2 = hi2 -> op <> AST_GREATER, op <> AST_LESS_EQUAL
      | _, 0, _ when lo1 = hi1 -> op <> AST_LESS, op <> AST_GREATER_EQUAL
      | _ -> true, true
end
