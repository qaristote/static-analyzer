(*
  Inspiré de :
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Signature of abstract domains representing sets of integers
  (for instance: constants or intervals).
 *)

open Ast

open Constrain

exception Error of string

module type VALUE_DOMAIN =
sig

  (* type of abstract elements *)
  (* an element of type t abstracts a set of integers *)
  type t

  (* unrestricted value: [-oo,+oo] *)
  val top: t

  (* bottom value: empty set *)
  val bottom: t

  (* constant: {c} *)
  val const: Z.t -> t

  (* interval: [a,b] *)
  val rand: Z.t -> Z.t -> t


  (* unary operation *)
  val unary: t -> int_unary_op -> t

  (* binary operation *)
  val binary: t -> t -> int_binary_op -> t


  (* comparison *)
  (* [compare x y op] returns (x',y') where
     - x' abstracts the set of v  in x such that v op v' is true for some v' in y
     - y' abstracts the set of v' in y such that v op v' is true for some v  in x
       i.e., we filter the abstract values x and y knowing that the test is true

     a safe, but not precise implementation, would be:
     compare x y op = (x,y)
  *)
  val compare: t -> t -> compare_op -> (t * t)


  (* backards unary operation *)
  (* [bwd_unary x op r] return x':
     - x' abstracts the set of v in x such as op v is in r
       i.e., we fiter the abstract values x knowing the result r of applying
       the operation on x
  *)
  val bwd_unary: t -> int_unary_op -> t -> t

  (* backward binary operation *)
  (* [bwd_binary x y op r] returns (x',y') where
     - x' abstracts the set of v  in x such that v op v' is in r for some v' in y
     - y' abstracts the set of v' in y such that v op v' is in r for some v  in x
       i.e., we filter the abstract values x and y knowing that, after
       applying the operation op, the result is in r
  *)
  val bwd_binary: t -> t -> int_binary_op -> t -> (t * t)


  (* set-theoretic operations *)
  val join: t -> t -> t
  val meet: t -> t -> t

  (* widening *)
  val widen: t -> t -> t

  (* subset inclusion of concretizations *)
  val subset: t -> t -> bool

  (* fmteck the emptiness of the concretization *)
  val is_bottom: t -> bool

  (* print abstract element *)
  val print: Format.formatter -> t -> unit

  (* evaluates a comparison between elements *)
  val eval_compare : compare_op -> t -> t -> bool * bool

  val get_constrain : t -> non_relational_constrain option
  val use_nr_constrain : non_relational_constrain -> t -> t
end

module Constants =
struct
  type t = Bot | Const of Z.t | Top

  let top = Top
  let bottom = Bot
  let zero = Const Z.zero
  let one = Const Z.one
  let minus_one = Const Z.minus_one

  let const (x : Z.t) : t = Const x
  let rand (m : Z.t) (n : Z.t) : t = if m = n then const m else top

  let join (m : t) (n : t) : t = match m, n with
    | Top, _ | _, Top -> Top
    | Bot, n | n, Bot -> n
    | Const x, Const y when x = y -> Const x
    | _ -> Top

  let meet (m : t) (n : t) : t = match m, n with
    | Bot, _ | _, Bot -> Bot
    | Top, a | a, Top -> a
    | Const x, Const y when x = y -> Const x
    | _ -> Bot

  let operator = function
    | AST_PLUS -> Z.add
    | AST_MINUS -> Z.sub
    | AST_MULTIPLY -> Z.mul
    | AST_DIVIDE -> Z.ediv
    | AST_MODULO -> Z.erem

  let unary (n : t) : int_unary_op -> t = function
    | AST_UNARY_PLUS -> n
    | AST_UNARY_MINUS -> match n with
      | Bot | Top -> n
      | Const x -> Const (Z.neg x)

  let binary (m : t) (n : t) : int_binary_op -> t = function
    | AST_PLUS | AST_MINUS as op ->
      begin
        match m, n with
        | Bot, _ | _, Bot -> Bot
        | Top, _ | _, Top -> Top
        | Const x, Const y -> Const ((operator op) x y)
      end
    | AST_MULTIPLY ->
      begin
        match m, n with
        | Bot, _ | _, Bot -> Bot
        | m, n when m = zero || n = zero -> zero
        | Top, _ | _, Top -> Top
        | Const x, Const y -> Const (Z.mul x y)
      end
    | AST_DIVIDE ->
      begin
        match m, n with
        | Bot, _ | _, Bot -> Bot
        | _, Top -> raise (Error "division by zero")
        | _, Const x when x = Z.zero -> raise (Error "division by zero")
        | Top, _ -> Top
        | Const x, Const y -> Const (Z.ediv x y)
      end
    | AST_MODULO ->
      begin
        match m, n with
        | Bot, _ | _, Bot -> Bot
        | _, Top -> raise (Error "modulo by zero")
        | _, Const x when x = Z.zero -> raise (Error "modulo by zero")
        | Top, _ -> Top
        | Const x, Const y -> Const (Z.erem x y)
      end

  let compare (m : t) (n : t) : compare_op -> t * t = function
    | AST_EQUAL -> let p = meet m n in p, p
    | AST_NOT_EQUAL | AST_LESS | AST_LESS_EQUAL | AST_GREATER | AST_GREATER_EQUAL as op ->
      begin
        match m, n with
        | Bot, _ | _, Bot -> Bot, Bot
        | Top, _ | _, Top -> m, n
        | Const x, Const y -> match Z.compare x y with
          | -1 ->
            (match op with
             | AST_NOT_EQUAL | AST_LESS | AST_LESS_EQUAL -> Const x
             | (* AST_GREATER | AST_GREATER_EQUAL *) _ -> Bot),
            (match op with
             | AST_NOT_EQUAL | AST_GREATER | AST_GREATER_EQUAL -> Const y
             | (* AST_LESS | AST_LESS_EQUAL *) _ -> Bot)
          | 0 ->
            (match op with
             | AST_NOT_EQUAL | AST_LESS | AST_GREATER -> Bot, Bot
             | (* AST_LESS_EQUAL | AST_GREATER_EQUAL *) _ -> Const x, Const y)
          | 1 ->
            (match op with
             | AST_NOT_EQUAL | AST_GREATER | AST_GREATER_EQUAL -> Const x
             | (* AST_LESS | AST_LESS_EQUAL *) _ -> Bot),
            (match op with
             | AST_NOT_EQUAL | AST_LESS | AST_LESS_EQUAL -> Const y
             | (* AST_GREATER | AST_GREATER_EQUAL *) _ -> Bot)
          | _ -> failwith "impossible"
      end

  let bwd_unary (n : t) (op : int_unary_op) (r : t) : t = match op with
    | AST_UNARY_PLUS -> meet n r
    | AST_UNARY_MINUS -> unary (meet (unary n AST_UNARY_MINUS) r) AST_UNARY_MINUS

  let bwd_binary (m : t) (n : t) (op :int_binary_op) (r : t) : t * t = match r with
    | Bot -> Bot, Bot
    | Top -> m, n
    | Const x -> match m, n with
      | Bot, _ | _, Bot -> Bot, Bot
      | Top, Top -> m, n
      | _ -> match op with
        | AST_PLUS | AST_MINUS | AST_MODULO ->
          begin
            match m, n with
            | Const y, Const z ->
              if (operator op) y z = x then
                m, n
              else
                Bot, Bot
            | (* Top, _ | _, Top *) _ -> m, n
          end
        | AST_MULTIPLY ->
          begin
            match m, n with
            | Top, Const y | Const y, Top when y = Z.zero && x = Z.zero ->
              m, n
            | Top, Const y when Z.rem x y = Z.zero && y <> Z.zero ->
              Const (Z.divexact x y), n
            | Const y, Top when Z.rem x y = Z.zero && y <> Z.zero ->
              m, Const (Z.divexact x y)
            | Const y, Const z when Z.mul y z = x ->
              m, n
            | _ ->
              Bot, Bot
          end
        | AST_DIVIDE ->
          begin
            match m, n with
            | Top, Const y -> Const (Z.mul x y), Const y
            | Const y, Top when x = Z.zero && y = Z.zero ->
              m, n
            | Const y, Top when x <> Z.zero && Z.rem y x = Z.zero ->
              m, Const (Z.divexact y x)
            | Const y, Const z when Z.mul x y = z ->
              m, n
            | _ ->
              Bot, Bot
          end

  let widen = join

  let subset (m : t) (n : t) : bool = match m, n with
    | Bot, _ | _, Top -> true
    | Top, _ | _, Bot -> false
    | Const x, Const y -> x = y

  let is_bottom : t -> bool = function
    | Bot -> true
    | _ -> false

  let print (fmt : Format.formatter) : t -> unit = function
    | Bot -> Format.fprintf fmt "∅"
    | Const x -> Format.fprintf fmt "{%a}" Z.pp_print x
    | Top -> Format.fprintf fmt "]-oo, +oo["

  let eval_compare (op : compare_op) (m : t) (n : t) : bool * bool = match m, n with
    | Bot, _ | _, Bot -> false, false
    | Top, _ | _, Top -> true, true
    | Const x, Const y -> match Z.compare x y, op with
      | -1, (AST_EQUAL | AST_GREATER | AST_GREATER_EQUAL)
      | 0, (AST_NOT_EQUAL | AST_LESS | AST_GREATER)
      | 1, (AST_EQUAL | AST_LESS | AST_LESS_EQUAL) -> false, true
      | _  (* -1, (AST_NOT_EQUAL | AST_LESS | AST_LESS_EQUAL)
              | 0, (AST_EQUAL | AST_LESS_EQUAL | AST_GREATER_EQUAL)
              | 1, (AST_NOT_EQUAL | AST_GREATER | AST_GREATER_EQUAL) *) -> true, false

  let get_constrain (v:t) : non_relational_constrain option =
    match v with
    | Bot | Top -> None
    | Const n -> Some (Constant n)

  let use_nr_constrain (nr_cons: non_relational_constrain) (cur_val: t) : t =
    match nr_cons with
    | Constant _ | Intervals _ | DisjointIntervals _ ->
      (* no reason to make these domains interact *) cur_val
end


open Interval

module Intervals : VALUE_DOMAIN =
struct
  include Interval

  let bwd_unary (i : t) (op : int_unary_op) (r : t) : t = match op with
    | AST_UNARY_PLUS -> meet i r
    | AST_UNARY_MINUS -> unary (meet (unary i AST_UNARY_MINUS) r) AST_UNARY_MINUS

  let widen (i : t) (j : t) : t =
    Interval.simplify (match i, j with
        | Interval.Empty, j -> j
        | i, Interval.Empty -> i
        | Between (lo1, hi1), Between (lo2, hi2) ->
          Between ((if Bound.compare lo1 lo2 <= 0 then
                      lo1 else
                      Bound.Inf),
                   (if Bound.compare hi1 hi2 >= 0 then
                      hi1 else
                      Bound.Sup)))

  let get_constrain (v:t) : non_relational_constrain option =
    match v with
    | Between (Int x,Int y) when x = y -> Some (Constant x)
    | _ -> Some (Intervals v)

  let use_nr_constrain (nr_cons: non_relational_constrain) (cur_val: t) : t =
    match nr_cons with
    | Constant _ | Intervals _ | DisjointIntervals _ ->
      (* no reason to make these domains interact *) cur_val
end

module DisjointIntervals : VALUE_DOMAIN =
struct
  type t = Interval.t list

  let top = [Interval.top]
  let bottom = []

  let const (n : Z.t) : t = [Interval.const n]

  let rand (m : Z.t) (n : Z.t) : t = [Interval.rand m n]

  let is_bottom: t -> bool = function
    | [] -> true
    | _ -> false

  let split (di : t) : t * t =
    let rec aux acc = function
      | [] -> List.rev (fst acc), List.rev (snd acc)
      | [i] -> i :: (fst acc), snd acc
      | i1 :: i2 :: is -> aux (i1 :: (fst acc), i2 :: (snd acc)) is in
    aux ([], []) di

  let join (di1 : t) (di2 : t) : t =
    let rec aux acc = function
      | [], [] -> acc
      | is, [] | [], is -> (List.rev acc) @ is
      | i1 :: is1, i2 :: is2 ->
        let m1 = Interval.inf i1
        and n1 = Interval.sup i1
        and m2 = Interval.inf i2
        and n2 = Interval.sup i2 in
        match Bound.compare m1 n2, Bound.compare n1 m2 with
        | x, y when x <= 0 && y <= 0 -> let new_is1, new_is2 = if Bound.compare n1 n2 >= 0 then is1, is2 else is2, is1 in
          aux acc ((Interval.join i1 i2) :: new_is1, new_is2)
        | 1, _ -> aux (i2 :: acc) ((i1 :: is1), is2)
        | _ -> aux (i1 :: acc) (is1, (i2 :: is2))
    in aux [] (di1, di2)

  let rec simplify (di : t) : t = match split di with
    | i, [] | [], i -> i
    | di1, di2 -> join (simplify di1) (simplify di2)

  let meet (di1 : t) (di2 : t) : t =
    let rec aux acc = function
      | [], _ | _, [] -> List.rev acc
      | i1 :: is1, i2 :: is2 ->
        aux
          (let i = (Interval.meet i1 i2) in
           if Interval.is_bottom i then
             acc
           else
             i :: acc)
          (if Bound.compare (Interval.sup i1) (Interval.sup i2) >= 0 then
             (i1 :: is1, is2)
           else
             (is1, i2 :: is2))
    in aux [] (di1, di2)

  let unary (di : t) : int_unary_op -> t = function
    | AST_UNARY_PLUS -> di
    | AST_UNARY_MINUS -> List.fold_left
                           (fun tmp i -> Interval.unary i AST_UNARY_MINUS :: tmp)
                           []
                           di

  let binary (di1 : t) (di2 : t) (op : int_binary_op) : t =
    List.fold_left
      (fun tmp1 i1 -> join
          tmp1
          (List.fold_left
             (fun tmp2 i2 -> join
                 tmp2
                 [Interval.binary i1 i2 op])
             []
             di2))
      []
      di1

  let compare (di1 : t) (di2 : t) (op : compare_op) : (t * t) =
    let multijoin (i11, i21) (i12, i22) = (join i11 i12, join i12 i22) in
    List.fold_left
      (fun tmp1 i1 ->
         multijoin tmp1
           (List.fold_left
              (fun tmp2 i2 ->
                 multijoin tmp2
                   (let new_i1, new_i2 = Interval.compare i1 i2 op in
                    [new_i1], [new_i2]))
              ([], [])
              di2))
      ([], [])
      di1

  let bwd_unary (di : t) (op : int_unary_op) (r : t) : t = match op with
    | AST_UNARY_PLUS -> meet di r
    | AST_UNARY_MINUS -> unary (meet (unary di AST_UNARY_MINUS) r) AST_UNARY_MINUS

  let bwd_binary (di1 : t) (di2 : t) (op : int_binary_op) (r : t) : (t * t) =
    let multijoin (i11, i21) (i12, i22) = (join i11 i12, join i12 i22) in
    List.fold_left
      (fun tmpr ir ->
         multijoin tmpr
           (List.fold_left
              (fun tmp1 i1 ->
                 multijoin tmp1
                   (List.fold_left
                      (fun tmp2 i2 ->
                         multijoin tmp2
                           (let new_i1, new_i2 = Interval.bwd_binary i1 i2 op ir in
                            [new_i1], [new_i2]))
                      ([], [])
                      di2))
              ([], [])
              di1))
      ([], [])
      r

  let widen (di1 : t) (di2 : t) : t =
    if is_bottom di1 then
      di2
    else (
      let union = join di1 di2 in
      let rec aux acc = function
        | [i1], [i2] ->
          List.rev ((if Interval.sup i1 <= Interval.sup i2 then
                       Interval.Between (Interval.inf i1, Bound.Sup)
                     else
                       i1) :: acc)
        | [i], i2 :: is2 -> aux (i2 :: acc) ([i], is2)
        | i1 :: is1, [i] -> aux acc (is1, [i])
        | i1 :: is1, i2 :: is2 -> aux (i2 :: acc) (is1, is2)
        | _ (* should not happen *) -> failwith "widen"
      in match di1, aux [] (di1, union) with
      | i1 :: is1, i2 :: is2 ->
        if Interval.inf i1 >= Interval.inf i2 then
          (Interval.Between (Bound.Inf, Interval.sup i1)) :: is2
        else
          union
      | _ (* should not happen *) -> failwith "widen")


  let subset (di1 : t) (di2 : t) : bool =
    List.for_all (fun i1 -> List.exists (Interval.subset i1) di2) di1

  let print: Format.formatter -> t -> unit = Printer.pp_list Interval.print " ∪ "

  let eval_compare (op : compare_op) (di1 : t) (di2 : t) : bool * bool =
    let orand (b11, b21) (b12, b22) = b11 || b12, b21 && b22 in
    List.fold_left
      (fun tmp1 i1 ->
         orand
           tmp1
           (List.fold_left
              (fun tmp2 i2 ->
                 orand
                   tmp2
                   (Interval.eval_compare op i1 i2))
              (false, true)
              di2))
      (false, true)
      di1

  let get_constrain (v:t) : non_relational_constrain option =
    match v with
    | [Between (Int x,Int y)] when x = y -> Some (Constant x)
    | _ -> Some (DisjointIntervals v)

  let use_nr_constrain (nr_cons: non_relational_constrain) (cur_val: t) : t =
    match nr_cons with
    | Constant _ | Intervals _ | DisjointIntervals _ ->
      (* no reason to make these domains interact *) cur_val
end
