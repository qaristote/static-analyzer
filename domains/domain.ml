(*
  Inspiré de :
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Signature of abstract domains representing sets of envrionments
  (for instance: a map from variable to their bounds).
 *)

open! Cfg
open Cfg_printer

open Constrain

exception Error of string

module type DOMAIN =
sig
  module C: CONSTRAIN

  (* type of abstract elements *)
  (* an element of type t abstracts a set of mappings from variables
     to integers
  *)
  type t

  (* initial environment, with all variables initialized to 0 *)
  val init: var list -> t

  (* empty set of environments *)
  val bottom: t

  (* set every variable to top *)
  val top: t -> t

  (* assign an integer expression to a variable *)
  val assign: t -> var -> int_expr -> t

  (* filter environments to keep only those satisfying the boolean expression *)
  val guard: t -> bool_expr -> t

  (* abstract join *)
  val join: t -> t -> t

  (* widening *)
  val widen: t -> t -> t

  (* whether an abstract element is included in another one *)
  val subset: t -> t -> bool

  (* whether the abstract element represents the empty set *)
  val is_bottom: t -> bool

  (* prints *)
  val print: Format.formatter -> t -> unit

  (* evaluates a boolean expression *)
  (* returns (can be true, can be false *)
  val eval_compare : t -> Ast.compare_op -> int_expr -> int_expr -> bool * bool

  val get_constrains: t -> C.t
  val use_constrains: C.t -> t -> t

end

module MakeProduct(A: DOMAIN)(B: DOMAIN with module C = A.C) : DOMAIN with module C = A.C =
struct
  module C = A.C

  type t = A.t * B.t
  let init (vl: var list) : t =
    A.init vl, B.init vl

  (* empty set of environments *)
  let bottom = A.bottom, B.bottom

  (* set every variable to top *)
  let top (enva,envb) = (A.top enva), (B.top envb)

  let reduce (enva: A.t) (envb: B.t) : t =
    let betterB = B.use_constrains (A.get_constrains enva) envb in
    let betterA = A.use_constrains (B.get_constrains betterB) enva in
    (betterA,betterB)

  (* assign an integer expression to a variable *)
  let assign ((enva,envb): t) (l: var) (e: int_expr)  : t =
    let enva = A.assign enva l e
    and envb = B.assign envb l e in
    reduce enva envb


  (* filter environments to keep only those satisfying the boolean expression *)
  let guard ((enva,envb): t) (e: bool_expr) : t =
    let enva = A.guard enva e
    and envb = B.guard envb e in
    (reduce enva envb)

  (* abstract join *)
  let join ((enva1,envb1):t) ((enva2,envb2):t) : t =
    reduce (A.join enva1 enva2) (B.join envb1 envb2)

  (* widening *)
  let widen ((enva1,envb1):t) ((enva2,envb2):t) : t =
    reduce (A.widen enva1 enva2) (B.widen envb1 envb2)

  (* whether an abstract element is included in another one *)
  let subset ((enva1,envb1):t) ((enva2,envb2):t) : bool =
    (A.subset enva1 enva2) && (B.subset envb1 envb2)

  (* whether the abstract element represents the empty set *)
  let is_bottom ((enva,envb):t) : bool =
    (A.is_bottom enva) && (B.is_bottom envb)

  (* prints *)
  let print (fmt:Format.formatter) ((enva,envb):t) : unit =
    A.print fmt enva; Format.pp_force_newline fmt (); B.print fmt envb

  (* evaluates a boolean expression *)
  let eval_compare ((enva,envb):t) (op: Ast.compare_op) (e1:int_expr) (e2:int_expr) : bool * bool =
    let can_be_true_a, can_be_false_a = A.eval_compare enva op e1 e2
    and can_be_true_b, can_be_false_b = B.eval_compare envb op e1 e2
    in
    (can_be_true_a && can_be_true_b), (can_be_false_a && can_be_false_b)

  let get_constrains ((enva,envb): t) : C.t =
    C.union (A.get_constrains enva) (B.get_constrains envb)

  let use_constrains (c:C.t) ((enva,envb): t) : t =
    let enva = A.use_constrains c enva
    and envb = B.use_constrains c envb in
    reduce enva envb
end

module MakeNonRelational(V : Value_domain.VALUE_DOMAIN) : DOMAIN with module C = SimpleContrains =
struct
  module C = SimpleContrains
  type t = V.t VarMap.t

  let init : var list -> t =
    List.fold_left (fun env var -> VarMap.add var (V.const Z.zero) env) VarMap.empty

  let bottom : t = VarMap.empty
  let top : t -> t = VarMap.map (fun _ -> V.top)

  let find_var var env = match VarMap.find_opt var env with
    | None -> V.bottom (* should only be possible is env = bottom *)
    | Some value -> value

  let rec eval_int_expr (env : t) : int_expr -> V.t = function
    | CFG_int_const n -> V.const n
    | CFG_int_rand (m, n) -> V.rand m n
    | CFG_int_var var -> find_var var env
    | CFG_int_unary (op, expr) -> V.unary (eval_int_expr env expr) op
    | CFG_int_binary (op, expr1, expr2) ->
      let v1 = eval_int_expr env expr1
      and v2 = eval_int_expr env expr2 in
      V.binary v1 v2 op

  let rec eval_compare (env : t) (op : Ast.compare_op) (expr1 : int_expr) (expr2 : int_expr) : bool * bool =
    V.eval_compare op (eval_int_expr env expr1) (eval_int_expr env expr2)

  let assign (env : t) (var : var) (expr : int_expr) : t =
    try
      VarMap.add var (eval_int_expr env expr) env
    with
    | Value_domain.Error s -> raise (Error s)

  let join : t -> t -> t =
    VarMap.merge (fun _ v1 v2 -> match v1, v2 with
        | None, None -> None
        | None, Some v | Some v, None -> Some v
        | Some v1, Some v2 -> Some (V.join v1 v2))

  let rec get_old_values (env : t) : int_expr -> V.t list = function
    | CFG_int_const n ->
      [V.const n]
    | CFG_int_rand (m, n) ->
      [V.rand m n]
    | CFG_int_var var -> [find_var var env]
    | CFG_int_unary (op, expr) ->
      let l = get_old_values env expr in
      (V.unary (List.hd l) op) :: l
    | CFG_int_binary (op, expr1, expr2) ->
      let l1 = get_old_values env expr1
      and l2 = get_old_values env expr2 in
      (V.binary (List.hd l1) (List.hd l2) op) ::
      (List.hd l1) ::
      (List.hd l2) ::
      ((List.tl l1) @ (List.tl l2))

  let rec specify_values (env : t) (old_values : V.t list) (new_value : V.t) : int_expr -> t * V.t list = function
    | CFG_int_const _ | CFG_int_rand _ ->
      env, List.tl old_values
    | CFG_int_var var ->
      VarMap.add var new_value env, List.tl old_values
    | CFG_int_unary (op, expr) ->
      specify_values env (List.tl old_values) (V.bwd_unary (List.hd old_values) op new_value) expr
    | CFG_int_binary (op, expr1, expr2) ->
      let old_value1 = List.hd old_values
      and old_value2 = List.nth old_values 1
      and old_values_left = List.tl (List.tl old_values) in
      let new_value1, new_value2 = V.bwd_binary old_value1 old_value2 op new_value in
      let tmp_env, tmp_old_values = specify_values env old_values_left new_value1 expr1 in
      specify_values tmp_env tmp_old_values new_value2 expr2

  let rec guard (env : t) : bool_expr -> t = function
    | CFG_bool_const true | CFG_bool_rand -> env
    | CFG_bool_const false -> bottom
    | CFG_bool_unary (_, expr) -> guard env (negate expr)
    | CFG_bool_binary (op, expr1, expr2) ->
      begin
        match op with
        | Ast.AST_AND -> guard (guard env expr1) expr2
        | Ast.AST_OR ->
          (* on prends en compte les effets de bord *)
          join
            (guard env expr1)
            (guard (guard env (negate expr1)) expr2)
      end
    | CFG_compare (op, expr1, expr2) ->
      (* on ne prends pas encore en compte les effets de bord *)
      let old_values1 = get_old_values env expr1
      and old_values2 = get_old_values env expr2 in
      let new_value1, new_value2 = V.compare (List.hd old_values1) (List.hd old_values2) op in
      let tmp_env, _ = specify_values env old_values1 new_value1 expr1 in
      (* il n'est pas nécessaire de recalculer old_values2 à
         partir de tmp_env car alpha o gamma o alpha = alpha *)
      fst (specify_values tmp_env old_values2 new_value2 expr2)

  let widen : t -> t -> t =
    VarMap.merge (fun _ v1 v2 -> match v1, v2 with
        | None, None -> None
        | None, Some v | Some v, None -> Some v
        | Some v1, Some v2 -> Some (V.widen v1 v2))



  let is_bottom : t -> bool =
    VarMap.for_all (fun _ v -> V.is_bottom v)

  let print (fmt : Format.formatter) (env : t) : unit =
    if VarMap.is_empty env then
      Format.fprintf fmt "⊥"
    else (
      Format.fprintf fmt "@[" ;
      VarMap.iter
        (fun var v -> Format.fprintf fmt "@[%a ∈ %a ; @]"
            print_var var
            V.print v)
        env ;
      Format.fprintf fmt "@]")

  let subset (env1 : t) (env2 : t) : bool =
    if is_bottom env2
    then is_bottom env1
    else
      VarMap.for_all (fun var v2 ->
          if VarMap.mem var env1 then
            V.subset (VarMap.find var env1) v2
          else
            V.is_bottom v2) env2

  let get_constrains (env: t) : C.t =
    VarMap.fold
      (fun var v cons_set ->
         match V.get_constrain v with
         | None -> cons_set
         | Some nr_cons ->
           C.add (NonRelational (var, nr_cons)) cons_set)
      env
      C.empty

  let use_constrains (cons_set: C.t) (env: t) : t =
    let use_one_constr c env =
      match c with
      | NonRelational (var, nr_cons) ->
        let cur_val = find_var var env in
        let new_val = V.use_nr_constrain nr_cons cur_val in
        VarMap.add var new_val env
      | Equal (var1, var2) ->
        let cur_val1 = find_var var1 env
        and cur_val2 = find_var var2 env in
        let new_val = V.meet cur_val1 cur_val2 in
        env |> VarMap.add var1 new_val |> VarMap.add var2 new_val
    in
    C.fold
      use_one_constr
      cons_set
      env

end

module Equality : DOMAIN with module C = SimpleContrains =
struct
  module C = SimpleContrains
  type equiv_class = VarSet.t ref
  type eq_env = equiv_class VarMap.t
  type t = Eq of eq_env | Bot


  let init (lst : var list) : t =
    match lst with
    | [] -> Bot
    | _ -> Eq (
        (* all variables are equal at the start *)
        let equiv_class = ref
            (List.fold_left
               (fun tmp var -> VarSet.add var tmp)
               (VarSet.empty)
               lst)
        in
        List.fold_left
          (fun env var -> VarMap.add var equiv_class env)
          (VarMap.empty)
          lst)

  let find_var var env : equiv_class * eq_env =
    match VarMap.find_opt var env with
    | None -> (* should only be possible is env = bottom ?*)
      let equiv = ref (VarSet.singleton var) in
      equiv, (VarMap.add var equiv env)
    | Some equiv -> equiv, env

  let bottom : t = Bot

  let top : t -> t = function
    | Bot -> Bot
    | Eq eq_env -> Eq (VarMap.mapi (fun var _ -> ref (VarSet.singleton var)) eq_env)

  let is_bottom : t -> bool = function
    | Bot -> true
    | Eq _ -> false


  let join (env1 : t) (env2 : t) : t =
    match env1, env2 with
    | Bot, env | env, Bot -> env
    | (Eq eq_env1), (Eq eq_env2) ->

      let join_env eq_env2 x equiv1 new_eq_env =
        if VarMap.mem x new_eq_env
        then new_eq_env (* already done *)
        else (* must be treated *)
          let equiv2,_ = find_var x eq_env2 in
          let equiv12 = ref (VarSet.inter !equiv1 !equiv2) in
          VarSet.fold
            (fun z new_eq_env -> VarMap.add z equiv12 new_eq_env)
            !equiv12
            new_eq_env
      in

      Eq (
        VarMap.empty |>
        VarMap.fold (join_env eq_env2) eq_env1 |>
        VarMap.fold (join_env eq_env1) eq_env2)


  let assign (env : t) (x : var) (expr : int_expr) : t =
    match env with
    | Bot -> Bot
    | Eq eq_env -> Eq (
        (* first remove x from its equality class *)
        (match VarMap.find_opt x eq_env with
         |Some old_equiv_class ->
           old_equiv_class := VarSet.remove x !old_equiv_class
         | None -> ());

        (* then add info about x if we have some *)
        match expr with
        | CFG_int_var y ->
          let equiv_class, eq_env = find_var y eq_env in
          equiv_class := VarSet.add x !equiv_class;
          VarMap.add x equiv_class eq_env
        | _ ->
          VarMap.add x (ref (VarSet.singleton x)) eq_env)


  let make_equal (x : var) (y : var) (eq_env : eq_env) : eq_env =
    let equiv_x, eq_env = find_var x eq_env in
    let equiv_y, eq_env = find_var y eq_env in
    equiv_x := VarSet.union !equiv_x !equiv_y;
    VarSet.fold (fun z env -> VarMap.add z equiv_x env) !equiv_y eq_env


  let rec guard (env : t) (expr : bool_expr) : t =
    match env with
    | Bot -> Bot
    | Eq eq_env ->
      match expr with
      | CFG_bool_const true | CFG_bool_rand -> env
      | CFG_bool_const false -> bottom
      | CFG_bool_unary (_, expr) -> guard env (negate expr)
      | CFG_bool_binary (op, expr1, expr2) ->
        begin
          match op with
          | Ast.AST_AND -> guard (guard env expr1) expr2
          | Ast.AST_OR ->
            (* on prends en compte les effets de bord *)
            join
              (guard env expr1)
              (guard (guard env (negate expr1)) expr2)
        end
      | CFG_compare (op, expr1, expr2) -> match expr1, expr2 with
        | CFG_int_var x, CFG_int_var y ->
          (match op with
           | Ast.AST_EQUAL ->
             Eq (make_equal x y eq_env)
           | Ast.AST_NOT_EQUAL | Ast.AST_LESS | Ast.AST_GREATER ->
             let equiv_x, eq_env = find_var x eq_env in
             if VarSet.mem y !equiv_x
             then env
             else bottom
           | _ -> env)
        | _ -> env


  let widen : t -> t -> t = join (* le nombre de variables est fini *)


  let subset (env1 : t) (env2 : t) : bool = match env1,env2 with
    | Bot, _ -> true
    | Eq _, Bot -> false
    | Eq eq_env1, Eq eq_env2 ->
      VarMap.for_all
        (fun var equiv_class2 ->
           let equiv_class1,_ = find_var var eq_env1 in
           VarSet.subset !equiv_class2 !equiv_class1)
        eq_env2


  let print (fmt : Format.formatter) (env : t) : unit =
    match env with
    | Bot ->
      Format.fprintf fmt "⊥"
    | Eq eq_env ->
      Format.fprintf fmt "@[" ;
      let add_var var  _ tmp = VarSet.add var tmp in
      let todo = ref (VarMap.fold add_var eq_env VarSet.empty) in
      while not (VarSet.is_empty !todo) do
        let var = VarSet.choose !todo in
        let equiv_class, _ = find_var var eq_env in
        Format.fprintf fmt "@[{ %a" print_var var ;
        todo := VarSet.remove var !todo ;
        VarSet.iter
          (fun equiv_var ->
             Format.fprintf fmt ", %a" print_var equiv_var ;
             todo := VarSet.remove equiv_var !todo)
          (VarSet.remove var !equiv_class) ;
        Format.fprintf fmt " }@] ; " ;
      done


  let rec eval_compare (env : t) (op : Ast.compare_op) (expr1 : int_expr) (expr2 : int_expr) : bool * bool =
    match env with
    | Bot -> false, false
    | Eq eq_env ->
      (match expr1, expr2 with
       | CFG_int_var var1, CFG_int_var var2 ->
         let equiv1, _ = find_var var1 eq_env in
         (match op with
          | AST_EQUAL ->
            true, not (VarSet.mem var2 !equiv1)

          | AST_NOT_EQUAL | AST_LESS | AST_GREATER ->
            not (VarSet.mem var2 !equiv1), true

          | _ -> true, true)
       | _ -> true, true)


  let get_constrains (env: t) : C.t =
    match env with
    | Bot -> C.empty
    | Eq eq_env ->
      (* NB: ici ajoute a la fois (x,y) et (y,x) *)
      VarMap.fold
        (fun x equiv_class_set_ref cons_set ->
           VarSet.fold
             (fun y cons_set ->
                if x <> y
                then C.add (Equal (x,y)) cons_set
                else cons_set)
             !equiv_class_set_ref
             cons_set)
        eq_env
        C.empty


  let make_equal_set (v_set : VarSet.t) (eq_env : eq_env) : eq_env =
    (* make all the var in the set equal in the env *)
    let equiv_class = ref
        (VarSet.fold
           (fun z eq_class ->
              let equiv, _ = find_var z eq_env in
              VarSet.union eq_class !equiv)
           v_set
           VarSet.empty)
    in
    VarSet.fold (fun z env -> VarMap.add z equiv_class env) !equiv_class eq_env


  let use_constrains (cons_set: C.t) (env: t) : t =
    match env with
    | Bot -> Bot
    | Eq eq_env ->  Eq (
        let const_val_hashtbl = Hashtbl.create 17 in

        let use_one_constr c = match c with
          | NonRelational(v, (Constant n)) ->
            (* put the constants in buckets *)
            begin match Hashtbl.find_opt const_val_hashtbl n with
              | Some s -> Hashtbl.replace const_val_hashtbl n (VarSet.add v s)
              | None -> Hashtbl.add const_val_hashtbl n (VarSet.singleton v)
            end

          | _ -> () (* no info *)

        in
        C.iter use_one_constr cons_set; (* build the bucket hashtbl *)

        (* update the environment *)
        Hashtbl.fold
          (fun _ ->  make_equal_set)
          const_val_hashtbl
          eq_env)

end
