open Cfg
open Interval

module type CONSTRAIN =
sig
  type c (* A constraint *)
  type t (* A set of contrains *)
  val empty: t
  val add: c -> t -> t
  val union: t -> t ->t
  val fold: (c -> 'acc -> 'acc) -> t -> 'acc -> 'acc
  val iter: (c -> unit) -> t -> unit
end

type non_relational_constrain =
  | Constant of Z.t
  | Intervals of Interval.t
  | DisjointIntervals of Interval.t list

type simple_constrain =
  | NonRelational of var * non_relational_constrain
  | Equal of var * var

module SimpleContrains : CONSTRAIN with type c = simple_constrain =
struct
  type c = simple_constrain
  module S = Set.Make(struct type t = c let compare = compare end)
  include S
end
